 
# Dev ssl generator 
Use devcert dependency to generate trusted ssl certificates for development use only.

The certs are unique for each machine so do not share the certs and keep them on git ignore.

## Run Locally  

Clone the project  

~~~bash  
  git clone https://gitlab.com/Oxoft/dev-ssl-generator.git
~~~

Go to the project directory  

~~~bash  
  cd my-project
~~~

Install dependencies  

~~~bash  
npm install
~~~

Start the server  

~~~bash  
npm run start
~~~
 

## License  

[MIT](https://choosealicense.com/licenses/mit/)
